import argparse
import subprocess
import os
import sys

parser = argparse.ArgumentParser(
    prog='clipper.py',
    description='Clip and squish videos down to the specified size quickly with minimal quality loss.',
    epilog='example usage: python3 clipper.py recordings/input.mkv clips/output.webm 4:20 69'
)

parser.add_argument(
    'INPUT',
    type=str,
    help='input filename'
)

parser.add_argument(
    'OUTPUT',
    type=str,
    help='output filename'
)

parser.add_argument(
    'START',
    help='video timestamp to start clipping from (e.g. 01:09:00 or 42:00)'
)

parser.add_argument(
    'DURATION',
    type=int,
    help='duration of clip in seconds'
)

parser.add_argument(
    '-s',
    '--size',
    default=25,
    type=float,
    nargs='?',
    help='target size in MB, default: 25'
)

parser.add_argument(
    '-v',
    '--video-codec',
    default='av1',
    type=str,
    choices=['av1', 'vp9'],
    nargs='?',
    help="video codec to use, default: av1"
)

args = parser.parse_args()

input_filename = args.INPUT
output_filename = args.OUTPUT
start = args.START
duration = args.DURATION
size = args.size
video_codec = args.video_codec
bitrate = round(size * 0.95 * 8 / duration)  # budget 5% for audio

if not os.path.exists('ffmpeg'):
    print('Uh oh, ffmpeg is missing. Put ffmpeg (static build) in this directory. Download: https://ffmpeg.org/')
    sys.exit(1)

if video_codec == 'av1':
    print('soz not yet')
    sys.exit(2)
elif video_codec == 'vp9':
    subprocess.call(
        [
            './ffmpeg',
            '-i', input_filename,
            '-ss', start,
            '-t', str(duration),
            '-c:v', 'libvpx-vp9',
            '-b:v', f'{bitrate}M',
            '-row-mt', '1',
            '-pass', '1',
            '-an',
            '-f', 'null',
            '/dev/null',
        ]
    )
    subprocess.call(
        [
            './ffmpeg',
            '-i', input_filename,
            '-ss', start,
            '-t', str(duration),
            '-c:v', 'libvpx-vp9',
            '-b:v', f'{bitrate}M',
            '-row-mt', '1',
            '-cpu-used', '2',
            '-pass', '2',
            '-c:a', 'libopus',
            '-b:a', '48k',
            output_filename
        ]
    )

if os.path.exists('ffmpeg2pass-0.log'):
    os.remove('ffmpeg2pass-0.log')

print('\nkthxbai')
